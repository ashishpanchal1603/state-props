import React, { Component } from "react";

export default class ClassComponent extends Component {
  render() {
    return <div className="classComponent">{this.props.count}</div>;
  }
}
