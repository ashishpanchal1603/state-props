import { useState } from "react";
import "./App.css";
import ClassComponent from "./ClassComponent";
import FunctionComponent from "./FunctionComponent";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
function App() {
  const [count, setCount] = useState(0);
  return (
    <>
      <div className="main-div">
        <div className="App">
          <ClassComponent count={count} />
          <FunctionComponent count={count} />
        </div>

        <div className="button ">
          <button
            onClick={() => setCount(count + 1)}
            className="btn btn-warning px-5 py-2 mx-2"
          >
            Increment{" "}
          </button>
          <button
            onClick={() => setCount(count - 1)}
            className="btn btn-danger px-5 py-2 mx-2"
          >
            {" "}
            decrement
          </button>
        </div>
      </div>
    </>
  );
}

export default App;
