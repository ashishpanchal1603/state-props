import React from "react";

function FunctionComponent({ count }) {
  return <div className="functionComponent">{count}</div>;
}

export default FunctionComponent;
